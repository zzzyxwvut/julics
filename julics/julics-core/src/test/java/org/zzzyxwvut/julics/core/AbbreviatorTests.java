package org.zzzyxwvut.julics.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.function.Function;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import org.zzzyxwvut.julics.core.DefaultLoggingConfiguration.Abbreviator;

public class AbbreviatorTests
{
	private Abbreviator abbreviator;

	@BeforeEach
	public void setUp()
	{
		abbreviator = new Abbreviator();
	}

	@SuppressWarnings("ThrowableResultIgnored")
	@Test
	public void testReduceWithNull()
	{
		final Function<Abbreviator, Executable> reducer =
							abbreviator_ -> () ->
			abbreviator_.reduce(null);
		assertThrows(NullPointerException.class,
						reducer.apply(abbreviator));
	}

	@Test
	public void testReduceWithEmptyString()
	{
		final String expected = "";
		final String obtained = abbreviator.reduce("");
		assertEquals(expected, obtained);
	}

	@Test
	public void testReduceWithx2ex2ex2ex2e()
	{
		final String expected = "....";
		final String obtained = abbreviator.reduce("....");
		assertEquals(expected, obtained);
	}

	@Test
	public void testReduceWithАзъ()
	{
		final String expected = "азъ";
		final String obtained = abbreviator.reduce("азъ");
		assertEquals(expected, obtained);
	}

	@Test
	public void testReduceWithазъx2eбукиx2eвѣдиx2eглагольx2e()
	{
		final String expected = "а.б.в.глаголь.";
		final String obtained = abbreviator.reduce(
					"азъ.буки.вѣди.глаголь.");
		assertEquals(expected, obtained);
	}

	@Test
	public void testReduceWithазъx2eбукиx2eвѣдиx2eглагольx2eРечьx24Слово()
	{
		final String expected = "а.б.в.г.Речь$Слово";
		final String obtained = abbreviator.reduce(
					"азъ.буки.вѣди.глаголь.Речь$Слово");
		assertEquals(expected, obtained);
	}

	@Test
	public void testReduceWithазъx2eбукиx2eвѣдиx2eглагольx2eРечьx2ex2ex2ex2e()
	{
		final String expected = "а.б.в.г.Речь....";
		final String obtained = abbreviator.reduce(
					"азъ.буки.вѣди.глаголь.Речь....");
		assertEquals(expected, obtained);
	}

	@Test
	public void testReduceWithаx2eбx2eвx2eгx2eРечьx2ex2ex2ex2e()
	{
		final String expected = "а.б.в.г.Речь....";
		final String obtained = abbreviator.reduce("а.б.в.г.Речь....");
		assertEquals(expected, obtained);
	}

	@Test
	public void testReduceWithРечь()
	{
		final String expected = "Речь";
		final String obtained = abbreviator.reduce("Речь");
		assertEquals(expected, obtained);
	}

	@Test
	public void testReduceWithРечьx2ex2ex2ex2e()
	{
		final String expected = "Речь....";
		final String obtained = abbreviator.reduce("Речь....");
		assertEquals(expected, obtained);
	}

	@Test
	public void testReduceWithx2ex2ex2ex2eРечь()
	{
		final String expected = "....Речь";
		final String obtained = abbreviator.reduce("....Речь");
		assertEquals(expected, obtained);
	}

	@Test
	public void testReduceWithазъx2eбукиx2ex2eглагольx2eРечь()
	{
		final String expected = "а.б..г.Речь";
		final String obtained = abbreviator.reduce(
					"азъ.буки..глаголь.Речь");
		assertEquals(expected, obtained);
	}

	@Test
	public void testReduceWithx2ex2ex2ex2eазъx2ex2eвѣдиx2ex2eРечьx2ex2ex2ex2e()
	{
		final String expected = "....а..в..Речь....";
		final String obtained = abbreviator.reduce(
					"....азъ..вѣди..Речь....");
		assertEquals(expected, obtained);
	}
}
