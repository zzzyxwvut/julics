package org.zzzyxwvut.julics.core;

import java.lang.management.ManagementFactory;
import java.lang.management.PlatformLoggingMXBean;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.Function;

import java.util.logging.LogManager;
import java.util.logging.Logger;

import org.zzzyxwvut.julics.core.internal.annotation.AcceptingPolicy;
import org.zzzyxwvut.julics.core.internal.annotation.RejectingPolicy;

/** This class makes a convenient use of {@link java.util.logging.LogManager}. */
public abstract class LoggingConfiguration
{
	private final Function<Class<?>, Runnable> listener = listener()
								.apply(this);

	/** Constructs a new {@code LoggingConfiguration} object. */
	protected LoggingConfiguration() { }

	private void manageLoggers(Class<?> klass)
	{
		/*
		 * Obtain (and define elsewhere) non-canonical class names for
		 * loggers so that dot-separated hierarchical namespaces would
		 * not clash with membered loggers.
		 */
		Logger logger = Logger.getLogger(klass.getName());

		/*
		 * Let a sire speak for his issue, e.g.
		 *	org.foo.bar.{Alpha,Beta}
		 *	org.foo
		 *	org
		 */
		while (logger != null) {
			onRead(logger);

			if (!logger.getUseParentHandlers())
				break;

			logger = logger.getParent();
		}
	}	/* See java.util.logging.Logger#log(LogRecord) */

	private static Function<LoggingConfiguration,
				Function<Class<?>, Runnable>> listener()
	{
		return that -> klass -> () -> that.manageLoggers(klass);
	}

	private static Function<String,
				BiFunction<String, String, String>> oldUpdater()
	{
		return property -> (oldValue, newValue) ->
					(oldValue == null && newValue == null)
			? null	/* Discard the property. */
			: (oldValue == null)
				? newValue
				: oldValue;
	}

	private static Function<String,
				BiFunction<String, String, String>> newUpdater()
	{
		return property -> (oldValue, newValue) ->
					(oldValue == null && newValue == null)
			? null	/* Discard the property. */
			: (newValue == null)
				? oldValue
				: newValue;
	}

	private void doUpdate(Function<String,
				BiFunction<String, String, String>> updater,
				Class<?> klass, String resource) throws
								IOException
	{
		Objects.requireNonNull(klass, "root klass");
		Objects.requireNonNull(resource, "resource name");

		try (InputStream is = klass.getResourceAsStream(resource)) {
			if (is == null)
				throw new IllegalArgumentException(
					String.format("Unavailable resource: '%s'",
								resource));

			LogManager.getLogManager()
				.addConfigurationListener(listener
					.apply(klass))
				.updateConfiguration(is, updater);
		}
	}

	/**
	 * Updates the logging configuration with properties parsed from the
	 * resource file and invokes the registered configuration listeners.
	 * All conflicting properties are resolved in favour of new non-null
	 * values, else old non-null values are retained; i.e. non-unique
	 * parsed configuration is accepted.
	 *
	 * @param klass the target class in relation to which the resource is
	 *	sought
	 * @param resource the name of the target resource
	 * @throws IOException if an I/O error occurs
	 * @throws SecurityException if a security manager is installed and it
	 *	denies an unspecified permission
	 * @see java.lang.Class#getResourceAsStream(String)
	 * @see #onRead(Logger)
	 */
	@AcceptingPolicy
	public final void updateWithNew(Class<?> klass, String resource)
							throws IOException
	{
		doUpdate(newUpdater(), klass, resource);
	}

	/**
	 * Updates the logging configuration with properties parsed from the
	 * resource file and invokes the registered configuration listeners.
	 * All conflicting properties are resolved in favour of old non-null
	 * values, else new non-null values are adopted; i.e. non-unique
	 * parsed configuration is rejected.
	 *
	 * @param klass the target class in relation to which the resource is
	 *	sought
	 * @param resource the name of the target resource
	 * @throws IOException if an I/O error occurs
	 * @throws SecurityException if a security manager is installed and it
	 *	denies an unspecified permission
	 * @see java.lang.Class#getResourceAsStream(String)
	 * @see #onRead(Logger)
	 */
	@RejectingPolicy
	public final void updateWithOld(Class<?> klass, String resource)
							throws IOException
	{
		doUpdate(oldUpdater(), klass, resource);
	}

	/**
	 * Performs an arbitrary action, whenever the logging configuration is
	 * read, for the passed {@code logger} and each ancestor logger or
	 * until a logger is found that does not send output to its parent.
	 *
	 * @param logger the target logger
	 * @see java.util.logging.LogManager#addConfigurationListener(Runnable)
	 * @see java.util.logging.Logger#setUseParentHandlers(boolean)
	 */
	protected abstract void onRead(Logger logger);

	/**
	 * Returns a list of the names of currently registered loggers.
	 *
	 * @return a list of the names of currently registered loggers
	 * @see java.lang.management.ManagementFactory#getPlatformMXBean(Class)
	 */
	public static List<String> getRegisteredLoggerNames()
	{
		return Optional.ofNullable(ManagementFactory
				.getPlatformMXBean(
					PlatformLoggingMXBean.class))
			.map(PlatformLoggingMXBean::getLoggerNames)
			.orElseGet(List::of);
	}
}
