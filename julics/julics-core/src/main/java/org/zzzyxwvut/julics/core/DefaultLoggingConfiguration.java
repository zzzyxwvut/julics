package org.zzzyxwvut.julics.core;

import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import java.util.logging.ConsoleHandler;
import java.util.logging.Handler;
import java.util.logging.LogManager;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/** A {@code LoggingConfiguration} that renders logger names reduced. */
public class DefaultLoggingConfiguration extends LoggingConfiguration
{
	/*
	 * Look for the "logger" argument in a value of the following property,
	 * e.g.
	 *
	 * java.util.logging.SimpleFormatter.format=%1$tT.%<tN %4$s [%3$s] %5$s%n%6$s
	 *
	 * where the positionally-indexed arguments, mapped with either %< or
	 * %\d$, are formatted as if by calling
	 *
	 * String.format(format, zonedDateTime, source, logger, level, message, thrown)
	 * // KEY:	"%...",	1$,		2$,	3$,	4$,	5$,	6$
	 *
	 * See java.logging/java.util.logging.SimpleFormatter#format(LogRecord)
	 * See java.base/java.util.Formatter
	 */
	private static final Pattern LOGGER_NAME_ENTRY =
					Pattern.compile(".*\\D3\\$s.*");

	private final Abbreviator abbreviator;

	/**
	 * Constructs a new {@code DefaultLoggingConfiguration} object.
	 *
	 * @param abbreviator an abbreviator
	 */
	public DefaultLoggingConfiguration(Abbreviator abbreviator)
	{
		this.abbreviator = Objects.requireNonNull(abbreviator,
							"abbreviator");
	}

	/**
	 * Constructs a new {@code DefaultLoggingConfiguration} object.
	 */
	public DefaultLoggingConfiguration()
	{
		this(new Abbreviator());
	}

	/** @see java.util.logging.SimpleFormatter#format(LogRecord) */
	private boolean hasLoggerNameEntry()
	{
		final String format = LogManager.getLogManager()
			.getProperty("java.util.logging.SimpleFormatter.format");
		return (format != null
			&& LOGGER_NAME_ENTRY.matcher(format).matches());
	}

	/**
	 * Makes every {@link SimpleFormatter} of a {@link ConsoleHandler},
	 * configured with this logger, abbreviate its fully-qualified logger
	 * name for {@link LogRecord}s according to the following pattern:
	 * <pre>
	 *	org -&gt; org
	 *	org.Foo -&gt; o.Foo
	 *	org.Bar$Baz -&gt; o.Bar$Baz
	 * </pre>
	 *
	 * @param logger the target logger
	 * @throws SecurityException if a security manager is installed and the
	 *	caller does not have {@code LoggingPermission("control")}
	 */
	@Override
	protected void onRead(Logger logger)
	{
		final String loggerName = logger.getName();

		if (loggerName == null || !loggerName.contains("."))
			return;

		for (Handler handler : logger.getHandlers()) {
			if (handler instanceof ConsoleHandler
					&& handler.getFormatter()
						instanceof SimpleFormatter
					&& hasLoggerNameEntry()) {
				/*
				 * Defer the "format" string retrieval until
				 * a logging configuration has been read.
				 *
				 * See java.logging/java.util.logging.SimpleFormatter
				 *	#format
				 * See java.base/jdk.internal.logger.SurrogateLogger
				 *	#getSimpleFormat(Function)
				 * See java.base/jdk.internal.logger.SimpleConsoleLogger$Formatting
				 *	#getSimpleFormat(String, Function)
				 */
				handler.setFormatter(new TerseSimpleFormatter(
								abbreviator));
			}
		}
	}

	/** This class offers a means of string abbreviation. */
	public static class Abbreviator
	{
		/**
		 * A compiled regular expression denoting a sequence of
		 * non-periods.
		 */
		protected static final Pattern NON_PERIODS =
					Pattern.compile("[^.]+");

		/**
		 * A compiled regular expression denoting the rightmost
		 * non-period sequence "word".
		 */
		protected static final Pattern RIGHTMOST_WORD =
					Pattern.compile("\\.([^.]+?)\\.*$");

		/** Constructs a new {@code Abbreviator} object. */
		public Abbreviator() { }

		/**
		 * Abbreviates all dot-separated words but the last one to
		 * their leftmost letters. E.g.
		 * <pre>
		 *	org -&gt; org
		 *	org.Foo -&gt; o.Foo
		 *	org.Bar$Baz -&gt; o.Bar$Baz
		 * </pre>
		 *
		 * @param name a string to abbreviate
		 * @return an abbreviated string or the passed string
		 */
		public String reduce(String name)
		{
			Objects.requireNonNull(name, "name");

			if (!name.contains("."))
				return name;

			final Matcher target = RIGHTMOST_WORD.matcher(name);

			if (!target.find())
				return name;

			final int anchor = target.start(1);
			final String head = name.substring(0, anchor);
			final String tail = name.substring(anchor);

			/*
			 * Note that Pattern#splitAsStream(CharSequence)
			 * won't cut it for it drops trailing empty elements.
			 */
			final String abbreviated = NON_PERIODS.matcher(head)
				.replaceAll(result -> result.group()
							.substring(0, 1));
			return new StringBuilder(abbreviated.length()
							+ tail.length())
				.append(abbreviated)
				.append(tail)
				.toString();
		}
	}

	private static class TerseSimpleFormatter extends SimpleFormatter
	{
		private final Abbreviator abbreviator;

		private TerseSimpleFormatter(Abbreviator abbreviator)
		{
			this.abbreviator = Objects.requireNonNull(abbreviator,
							"abbreviator");
		}

		@Override
		public String format(LogRecord record)
		{
			final String name = record.getLoggerName();

			if (name != null && name.contains("."))
				record.setLoggerName(abbreviator.reduce(name));

			return super.format(record);
		}	/* Points to the "format" string found at creation. */
	}
}
