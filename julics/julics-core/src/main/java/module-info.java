/** Defines a means of {@link java.util.logging.LogManager} configuration. */
@SuppressWarnings("module")
module org.zzzyxwvut.julics.core
{
	requires java.management;

	requires transitive java.logging;

	exports org.zzzyxwvut.julics.core;

	exports org.zzzyxwvut.julics.core.internal.annotation to
		org.zzzyxwvut.julics.annotations;
}
