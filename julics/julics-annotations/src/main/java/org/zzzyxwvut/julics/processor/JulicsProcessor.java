package org.zzzyxwvut.julics.processor;

import static org.zzzyxwvut.julics.processor.JulicsProcessor.LOGGABLE;
import static org.zzzyxwvut.julics.processor.SupportedStrategy.ProcessorOption.OPTION_ASCRIPTIVE;
import static org.zzzyxwvut.julics.processor.SupportedStrategy.ProcessorOption.OPTION_BUILD_TIME;
import static org.zzzyxwvut.julics.processor.SupportedStrategy.ProcessorOption.OPTION_CONCURRENT;
import static org.zzzyxwvut.julics.processor.SupportedStrategy.ProcessorOption.OPTION_VERBOSE;

import java.io.FileDescriptor;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.io.Writer;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.ArrayDeque;
import java.util.Collections;
import java.util.HashSet;
import java.util.Map.Entry;
import java.util.Map;
import java.util.Objects;
import java.util.Queue;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedOptions;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.AnnotationValue;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementVisitor;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.util.ElementFilter;
import javax.lang.model.util.ElementKindVisitor9;
import javax.lang.model.util.Elements;
import javax.lang.model.util.SimpleElementVisitor9;
import javax.tools.Diagnostic;

import org.zzzyxwvut.julics.processor.SupportedStrategy.Context;
import org.zzzyxwvut.julics.processor.SupportedStrategy.Item;
import org.zzzyxwvut.julics.processor.SupportedStrategy.ProcessorOption;

/**
 * This {@code AbstractProcessor} attends to the {@code Loggable} type.
 * <p>
 * For every class (barring local and anonymous classes) annotated with
 * the supported annotation, a new source file shall be generated for
 * the purpose of updating logging configuration with parsed properties.
 */
@SupportedAnnotationTypes("org.zzzyxwvut.julics.annotations/" + LOGGABLE)
@SupportedOptions({
	OPTION_ASCRIPTIVE, OPTION_BUILD_TIME, OPTION_CONCURRENT, OPTION_VERBOSE
})
@SupportedSourceVersion(SourceVersion.RELEASE_11)
public class JulicsProcessor extends AbstractProcessor
{
	/* Constant expressions are required for annotation element values. */
	/** The name of the {@code Loggable} annotation. */
	static final String LOGGABLE = "org.zzzyxwvut.julics.annotation.Loggable";

	private ProcessorOption options;
	private Function<Function<TypeElement, Stream<Item>>,
			Function<Set<? extends TypeElement>, Queue<Item>>>
								collector;
	private Function<RoundEnvironment,
			Function<TypeElement, Stream<Item>>> finder;
	private Consumer<Queue<Item>> delegator;

	/** Constructs a new {@code JulicsProcessor} object. */
	public JulicsProcessor() { super(); }

	@Override
	public void init(ProcessingEnvironment processingEnv)
	{
		super.init(processingEnv);
		options = new ProcessorOption(processingEnv.getOptions());

		if (options.verbose)
			processingEnv
				.getMessager()
				.printMessage(Diagnostic.Kind.OTHER,
					"PROCESSOR OPTIONS: ".concat(
						processingEnv.getOptions()
							.toString()));

		collector = collector()
			.apply(options.concurrent)
			.apply((options.ascriptive && options.verbose)
				? reporter()
					.apply(processingEnv
						.getElementUtils())
					.apply(new FileWriter(
						FileDescriptor.err))
				: item -> { });
		finder = finder()
			.apply(elementFilter()
				.apply(new ClassKindVisitor()))
			.apply(seeker()
				.apply(mirrorFilter()
					.apply(new ElementTypeElementVisitor()))
				.apply(mirrorValuer()
					.apply(processingEnv))
				.apply(processingEnv
					.getElementUtils()));
		final Context context = new Context(this, options,
							processingEnv);
		delegator = delegator()
			.apply(options.concurrent)
			.apply(strategist()
				.apply(new NoOpSupporter(context))
				.apply(Map.of(LOGGABLE,
					new LoggableSupporter(context))));
	}

	@Override
	public Set<String> getSupportedAnnotationTypes()
	{
		if (!options.ascriptive)
			return super.getSupportedAnnotationTypes();

		final String moduleName = Objects.requireNonNullElse(
				JeNeSaisQuoi.class.getModule().getName(), "");
		final String dummyType = String.format("%s%s%s", moduleName,
			(moduleName.isEmpty()) ? "" : "/",
			JeNeSaisQuoi.class.getCanonicalName());
		final Set<String> types = new HashSet<>(
				super.getSupportedAnnotationTypes());
		types.add(dummyType);
		return Collections.unmodifiableSet(types);
	} /* See javax.annotation.processing.AbstractProcessor#arrayToSet(String[], boolean). */

	private static Function<ProcessingEnvironment,
				Function<AnnotationMirror,
					Stream<Entry<ExecutableElement,
						AnnotationValue>>>>
								mirrorValuer()
	{
		return processingEnv -> mirror -> Map.<ExecutableElement,
							AnnotationValue>copyOf(
								processingEnv
				.getElementUtils()
				.getElementValuesWithDefaults(mirror))
			.entrySet()
			.stream();
	}

	private static Function<ElementVisitor<Boolean, TypeElement>,
				Function<TypeElement,
				Predicate<AnnotationMirror>>> mirrorFilter()
	{
		return typeElementVisitor -> roundAnnotation -> mirror ->
						mirror.getAnnotationType()
			.asElement()
			.accept(typeElementVisitor, roundAnnotation);
	}

	private static Function<Function<TypeElement,
					Predicate<AnnotationMirror>>,
				Function<Function<AnnotationMirror,
					Stream<Entry<ExecutableElement,
							AnnotationValue>>>,
				Function<Elements,
				Function<TypeElement,
				Function<TypeElement, Stream<Item>>>>>> seeker()
	{
		return mirrorFilter -> mirrorValuer -> elements ->
					roundAnnotation -> annotatedType ->
			Stream.of(new Item(roundAnnotation, annotatedType,
				elements.getAllAnnotationMirrors(annotatedType)
					.stream()
					.unordered() /* Non-repeatable. */
					.filter(mirrorFilter
						.apply(roundAnnotation))
					.flatMap(mirrorValuer)
					.collect(Collectors.toUnmodifiableMap(
						entry -> entry.getKey()
								.getSimpleName()
								.toString(),
						entry -> entry.getValue()
								.toString()))));
	}

	private static Function<ElementVisitor<Boolean, Void>,
				Predicate<TypeElement>> elementFilter()
	{
		return classVisitor -> element -> element
			.accept(classVisitor, null);
	}

	private static Function<Predicate<TypeElement>,
				Function<Function<TypeElement,
					Function<TypeElement, Stream<Item>>>,
				Function<RoundEnvironment,
				Function<TypeElement, Stream<Item>>>>> finder()
	{
		return elementFilter -> seeker -> roundEnv -> roundAnnotation ->
				ElementFilter.typesIn(roundEnv
					.getElementsAnnotatedWith(
							roundAnnotation))
			.stream()
			.unordered()
			.filter(elementFilter)
			.flatMap(seeker
				.apply(roundAnnotation));
	}

	private static Function<Elements,
				Function<Writer, Consumer<Item>>> reporter()
	{
		return elements -> writer -> item -> {
			try {
				writer.flush();
				elements.printElements(writer,
						item.annotatedTypeElement);
			} catch (final IOException e) {
				throw new UncheckedIOException(e);
			}
		};
	}

	private static <T> Stream<T> concurrentOrSequential(Stream<T> stream,
							boolean concurrent)
	{
		return (concurrent)
			? stream.parallel()
			: stream.sequential();
	}

	private static Function<Boolean,
				Function<Consumer<Item>,
				Function<Function<TypeElement, Stream<Item>>,
				Function<Set<? extends TypeElement>,
						Queue<Item>>>>> collector()
	{
		return concurrent -> reporter -> finder -> roundAnnotations ->
				concurrentOrSequential(roundAnnotations.stream(),
								concurrent)
			.unordered()
			.flatMap(finder)
			.peek(reporter)
			.collect(Collectors.toCollection(ArrayDeque::new));
	}

	private boolean doProcess(Set<? extends TypeElement> annotations,
						RoundEnvironment roundEnv)
	{
		final Queue<Item> agenda = collector
			.apply(finder
				.apply(roundEnv))
			.apply(annotations);

		if (options.verbose)
			processingEnv
				.getMessager()
				.printMessage(Diagnostic.Kind.OTHER,
					"AGENDA: ".concat(
						agenda.toString()));

		delegator.accept(agenda);
		return true;
	}

	@Override
	public boolean process(Set<? extends TypeElement> annotations,
						RoundEnvironment roundEnv)
	{
		return (roundEnv.processingOver()
			|| roundEnv.errorRaised()
			|| doProcess(annotations, roundEnv));
	}

	private static Function<Boolean,
				Function<Consumer<Item>,
				Consumer<Queue<Item>>>> delegator()
	{
		return concurrent -> strategist -> agenda ->
				concurrentOrSequential(agenda.stream(),
								concurrent)
			.forEach(strategist);
	}

	private static Function<SupportedStrategy,
				Function<Map<String, SupportedStrategy>,
				Consumer<Item>>> strategist()
	{
		return noop -> strategies -> item ->
				Objects.requireNonNullElse(strategies.get(
					item.annotationElement.toString()),
					noop)
			.supply(item);
	}

	private static final class NoOpSupporter extends SupportedStrategy
	{
		private NoOpSupporter(Context context)	{ super(context); }

		@Override
		void supply(Item item)
		{
			Objects.requireNonNull(item, "item");
			context.processingEnv.getMessager()
				.printMessage(Diagnostic.Kind.WARNING,
					String.format("%s: NO-OP SUPPORTER",
						item.annotationElement),
					item.annotatedTypeElement);
		}
	}

	private static class ClassKindVisitor extends
					ElementKindVisitor9<Boolean, Void>
	{
		private ClassKindVisitor() { super(); }

		@Override
		protected Boolean defaultAction(Element e, Void v)
		{
			return Boolean.FALSE;
		}

		@Override
		public Boolean visitTypeAsClass(TypeElement e, Void v)
		{
			return Boolean.TRUE;
		}

		@Override
		public Boolean visitTypeAsEnum(TypeElement e, Void v)
		{
			return Boolean.TRUE;
		}
	}

	private static class ElementTypeElementVisitor extends
				SimpleElementVisitor9<Boolean, TypeElement>
	{
		private ElementTypeElementVisitor() { super(); }

		@Override
		protected Boolean defaultAction(Element e0, TypeElement e1)
		{
			return Boolean.FALSE;
		}

		@Override
		public Boolean visitType(TypeElement e0, TypeElement e1)
		{
			return e0.getClass().equals(e1.getClass())
				&& e0.getQualifiedName()
					.equals(e1.getQualifiedName());
		}	/* See java.compiler/javax.lang.model.element.Name. */
	}

	/** This is a nonce annotation to be used for testing. */
	@Target(ElementType.TYPE)
	@Retention(RetentionPolicy.SOURCE)
	public @interface JeNeSaisQuoi
	{
		/**
		 * Returns {@code true}.
		 *
		 * @return {@code true}
		 */
		boolean none() default true;

		/**
		 * Returns an empty string.
		 *
		 * @return an empty string
		 */
		String comment() default "";
	}
}
