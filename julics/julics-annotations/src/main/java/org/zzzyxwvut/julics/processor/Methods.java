package org.zzzyxwvut.julics.processor;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Objects;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/** This class lends method-related support. */
class Methods
{
	private Methods() { /* No instantiation. */ }

	/**
	 * Collects the set of declared method names of a {@code klass}.
	 *
	 * @param klass a class
	 *
	 * @return the set of declared method names of a {@code klass}
	 */
	static Set<String> methodNames(Class<?> klass)
	{
		Objects.requireNonNull(klass, "klass");
		return Arrays.stream(klass.getDeclaredMethods())
			.map(Method::getName)
			.collect(Collectors.toUnmodifiableSet());
	}

	/**
	 * Tests whether there are as many {@code predicate} as formal
	 * parameters of a {@code method}, and evaluates them all.
	 *
	 * @param method a method
	 * @param predicate a series of predicates
	 * @return {@code false}, unless for every formal parameter of
	 *	a {@code method} there is a {@code predicate} that evaluates
	 *	{@code true}
	 */
	@SafeVarargs
	static boolean isParameterCompatible(Method method,
					Predicate<Class<?>>... predicate)
	{
		Objects.requireNonNull(method, "method");
		final Class<?>[] parameterTypes = method.getParameterTypes();

		if (predicate == null) {
			return (parameterTypes.length == 0);
		} else if (predicate.length != parameterTypes.length) {
			return false;
		}

		final Iterator<Class<?>> types = Arrays
			.asList(parameterTypes)
			.iterator();

		for (Predicate<Class<?>> tester : predicate)
			if (tester == null || !tester.test(types.next()))
				return false;

		return true;
	}
}
