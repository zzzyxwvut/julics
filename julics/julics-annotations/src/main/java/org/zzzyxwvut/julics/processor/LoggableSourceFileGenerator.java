package org.zzzyxwvut.julics.processor;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UncheckedIOException;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.Objects;

import javax.lang.model.element.PackageElement;
import javax.lang.model.element.TypeElement;
import javax.tools.Diagnostic;
import javax.tools.JavaFileObject;

import org.zzzyxwvut.julics.naming.internal.ClassNamer;
import org.zzzyxwvut.julics.processor.SupportedStrategy.Context;

/** This class serves for generation of logger configuration source files. */
final class LoggableSourceFileGenerator
{
	private final LoggableSupporter supporter;
	private final Context context;
	private final TypeElement annotatedType;

	/**
	 * Constructs a new {@code LoggableSourceFileGenerator} object.
	 *
	 * @param supporter a {@code Loggable} supported strategy
	 * @param context a processing context
	 * @param annotatedType an annotated type element
	 */
	LoggableSourceFileGenerator(LoggableSupporter supporter,
			Context context, TypeElement annotatedType)
	{
		this.supporter = Objects.requireNonNull(supporter,
							"supporter");
		this.context = Objects.requireNonNull(context, "context");
		this.annotatedType = Objects.requireNonNull(annotatedType,
							"annotatedType");
	}

	private JavaFileObject newJFO(String newQualifiedName) throws
								IOException
	{
		synchronized (LoggableSourceFileGenerator.class) {
			return context.processingEnv.getFiler()
				.createSourceFile(newQualifiedName,
							annotatedType);
		}
	}

	/**
	 * Generates a logger configuration source file.
	 *
	 * @param processorName the name of an annotation processor
	 * @param classBody a string representation of a class body
	 * @see #shapeClassBody(Map)
	 */
	void generateSourceFile(String processorName, String classBody)
	{
		final String fullClassName = annotatedType.getQualifiedName()
			.toString(); /* Empty for local and anonymous ones. */
		final String newQualifiedName = ClassNamer.give(fullClassName);
		final String newNonQualifiedName = ClassNamer.simpleGivenName(
							newQualifiedName);

		try {
			final JavaFileObject fileObject = newJFO(
							newQualifiedName);

			try (OutputStream os = fileObject.openOutputStream();
					OutputStreamWriter osw =
						new OutputStreamWriter(os,
							StandardCharsets.UTF_8);
					BufferedWriter bw =
						new BufferedWriter(osw)) {
				final PackageElement pe = context.processingEnv
						.getElementUtils()
						.getPackageOf(annotatedType);

				if (!pe.isUnnamed()) {
					bw.write("package ");
					bw.write(pe.getQualifiedName()
								.toString());
					bw.write(";");
					bw.newLine();
					bw.newLine();
				}

				bw.write("@javax.annotation.processing.Generated");
				bw.write("(");

				if (context.options.buildTime != null) {
					bw.write("date = \"");
					bw.write(context.options.buildTime);
					bw.write("\",");
				}

				bw.newLine();
				bw.write("\tvalue = \"");
				bw.write(processorName);
				bw.write("\")");
				bw.newLine();
				bw.write("class ");
				bw.write(newNonQualifiedName);
				bw.newLine();
				bw.write(classBody);
			}
		} catch (final IOException e) {
			throw new UncheckedIOException(e);
		}

		if (context.options.verbose)
			context.processingEnv
				.getMessager()
				.printMessage(Diagnostic.Kind.NOTE,
						"DONE GENERATING...",
						annotatedType);
	}	/* maven-compiler-plugin: MCOMPILER-66 */

	/**
	 * Returns a string representation of a class body.
	 *
	 * @param elements a map that associates an annotation's elements with
	 *	their corresponding values, either declared or default
	 * @return a string representation of a class body
	 */
	String shapeClassBody(Map<String, String> elements)
	{
		final boolean retainsValues = Boolean.parseBoolean(
						Objects.requireNonNull(
				elements.get(supporter.retainsValuesName()),
				"no retainsValuesName key"));
		final String resource = Objects.requireNonNull(
				elements.get(supporter.resourceName()),
				"no resourceName key");
		final String configClass = Objects.requireNonNull(
				elements.get(supporter.configClassName()),
				"no configClassName key");
		final String configType = configClass.substring(0,
						configClass.lastIndexOf('.'));
		final String loggerClass = String.format("%s.class",
				annotatedType.getQualifiedName().toString());
		return String.format("{%n%1$s"
						+ "\t%2$s%n"
						+ "\t\t%3$s%n"
						+ "\t\t\t%4$s%n"
						+ "\t\t%5$s%n"
						+ "\t\t\t%6$s%n"
						+ "\t\t%7$s%n"
						+ "\t%8$s%n}%n",
			(context.options.ascriptive)
				? String.format("\t%s%b;%n%n",
					"static final boolean R = ",
					retainsValues)
				: "",
			"static {",
			"try {",
			String.format((retainsValues)
					? supporter.getRejectorTemplet()
					: supporter.getAcceptorTemplet(),
				configType, loggerClass, resource),
			"} catch (final java.io.IOException e) {",
			"throw new ExceptionInInitializerError(e);",
			"}",
			"}");
	}
}
