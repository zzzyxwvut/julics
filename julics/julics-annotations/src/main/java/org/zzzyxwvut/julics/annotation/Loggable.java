package org.zzzyxwvut.julics.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.zzzyxwvut.julics.core.DefaultLoggingConfiguration;
import org.zzzyxwvut.julics.core.LoggingConfiguration;

/**
 * This annotation manages updating of the logging configuration with
 * properties parsed from the resource file and invocation of the registered
 * configuration listeners for the target class logger.
 * <p>
 * (Local and anonymous classes, all classes of an unnamed package in a named
 * module, are <strong>not</strong> supported.)
 *
 * @see LoggingConfiguration#updateWithNew(Class, String)
 * @see LoggingConfiguration#updateWithOld(Class, String)
 * @see org.zzzyxwvut.julics.naming.LoggerRef
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.SOURCE)
@Documented
public @interface Loggable
{
	/**
	 * Confirms that for any conflicting non-null property value an old
	 * (current) value is retained and a new (parsed from the resource
	 * file) value is ignored. The default value is {@code true}.
	 *
	 * @return whether to favour old non-null or new non-null values
	 */
	boolean retainsValues() default true;

	/**
	 * Gets the logging configuration class. It is assumed of the class to
	 * define a public constructor with no formal parameters. The default
	 * value is the {@link DefaultLoggingConfiguration} class.
	 *
	 * @return the logging configuration class
	 */
	Class<? extends LoggingConfiguration> configClass() default
					DefaultLoggingConfiguration.class;

	/**
	 * Gets the name of the file resource. The default value is
	 * the "/logging.properties" string.
	 *
	 * @return the name of the file resource
	 * @see java.lang.Class#getResourceAsStream(String)
	 */
	String resource() default "/logging.properties";
}
