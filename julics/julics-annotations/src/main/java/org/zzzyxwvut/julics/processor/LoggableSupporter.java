package org.zzzyxwvut.julics.processor;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.zzzyxwvut.julics.annotation.Loggable;
import org.zzzyxwvut.julics.core.LoggingConfiguration;
import org.zzzyxwvut.julics.core.internal.annotation.AcceptingPolicy;
import org.zzzyxwvut.julics.core.internal.annotation.RejectingPolicy;

/** A {@code SupportedStrategy} that supports {@code Loggable}. */
final class LoggableSupporter extends SupportedStrategy
{
	private static final String ACCEPTOR_TEMPLET;
	private static final String REJECTOR_TEMPLET;

	static {
		/*
		 * A handful of paranoid tests to catch incompatible
		 * dependency refactoring.
		 */
		final Set<String> expected = Set.of("configClass",
							"resource",
							"retainsValues");
		final Set<String> elements = Methods.methodNames(
							Loggable.class);

		if (!elements.containsAll(expected))	/* Ignore new ones. */
			throw new IncompatibleClassChangeError(
					"Required methods not found");

		final String templet = "new %%1$s()%%n"
						+ "\t\t\t\t.%s(%%2$s,%%n"
						+ "\t\t\t\t\t\t%%3$s);";
		final Map<String, String> policyTemplets = fetchPolicies(
								templet);

		if (policyTemplets.size() != 2)
			throw new IncompatibleClassChangeError(
					"Method signature/modifier change");

		ACCEPTOR_TEMPLET = Objects.requireNonNull(policyTemplets.get(
				AcceptingPolicy.class.getSimpleName()),
				"no AcceptingPolicy method target");
		REJECTOR_TEMPLET = Objects.requireNonNull(policyTemplets.get(
				RejectingPolicy.class.getSimpleName()),
				"no RejectingPolicy method target");
	}

	/**
	 * Constructs a new {@code LoggableSupporter} object.
	 *
	 * @param context a processing context
	 */
	LoggableSupporter(Context context)	{ super(context); }

	/**
	 * Returns the "configClass" string.
	 *
	 * @return the "configClass" string
	 */
	String configClassName()	{ return "configClass"; }

	/**
	 * Returns the "resource" string.
	 *
	 * @return the "resource" string
	 */
	String resourceName()		{ return "resource"; }

	/**
	 * Returns the "retainsValues" string.
	 *
	 * @return the "retainsValues" string
	 */
	String retainsValuesName()	{ return "retainsValues"; }

	/**
	 * Returns the accepting policy templet string.
	 *
	 * @return the accepting policy templet string
	 */
	String getAcceptorTemplet()	{ return ACCEPTOR_TEMPLET; }

	/**
	 * Returns the rejecting policy templet string.
	 *
	 * @return the rejecting policy templet string
	 */
	String getRejectorTemplet()	{ return REJECTOR_TEMPLET; }

	@Override
	void supply(Item item)
	{
		Objects.requireNonNull(item, "item");
		final LoggableSourceFileGenerator generator =
			new LoggableSourceFileGenerator(this, context,
						item.annotatedTypeElement);
		final String classBody = generator.shapeClassBody(
							item.elements);
		final String processorName = Objects.requireNonNull(
						context.processor.getClass()
							.getCanonicalName(),
						"<no canonical name>");
		generator.generateSourceFile(processorName, classBody);
	}

	@Override
	public String toString()
	{
		return new StringBuilder(getAcceptorTemplet().length()
				+ getRejectorTemplet().length() + 32 + 10)
			.append("(")
			.append(configClassName())	/* 11 chars */
			.append(", ")
			.append(resourceName())		/*  8 chars */
			.append(", ")
			.append(retainsValuesName())	/* 13 chars */
			.append(")")
			.append(System.lineSeparator())
			.append(getAcceptorTemplet())
			.append(System.lineSeparator())
			.append(getRejectorTemplet())
			.toString();
	}

	/*
	 * Returns a map with keys derived from the method names of
	 * LoggingConfiguration that are annotated with either AcceptingPolicy
	 * or RejectingPolicy and parameter-compliant, and with values as
	 * java.util.Formatter style templets derived from the passed
	 * argument.
	 */
	private static Map<String, String> fetchPolicies(String templet)
	{
		return Arrays.stream(LoggingConfiguration.class
							.getDeclaredMethods())
			.filter(method -> method.isAnnotationPresent(
						AcceptingPolicy.class)
				^ method.isAnnotationPresent(
						RejectingPolicy.class)
				&& Methods.isParameterCompatible(method,
						Class.class::equals,
						String.class::equals))
			.collect(Collectors.groupingBy(method ->
				(method.isAnnotationPresent(
						AcceptingPolicy.class)
					? AcceptingPolicy.class
					: RejectingPolicy.class)
						.getSimpleName(),
				Collectors.flatMapping(templeteer()
							.apply(templet),
						Collectors.joining())));
	}

	private static Function<String,
				Function<Method, Stream<String>>> templeteer()
	{
		return templet -> method -> Stream.of(String.format(templet,
							method.getName()));
	}
}
