package org.zzzyxwvut.julics.processor;

import java.util.Map;
import java.util.Objects;

import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.Processor;
import javax.lang.model.element.TypeElement;

/** This class serves for accommodation of specific processing schemes. */
abstract class SupportedStrategy
{
	/** A processing context. */
	final Context context;

	/**
	 * Constructs a new {@code SupportedStrategy} object.
	 *
	 * @param context a processing context
	 */
	SupportedStrategy(Context context)
	{
		this.context = Objects.requireNonNull(context, "context");
	}

	/**
	 * Supplies support.
	 *
	 * @param item an item to provide for
	 */
	abstract void supply(Item item);

	/**
	 * This class serves for aggregation of a supported annotation
	 * instance, an annotated type instance, and the elements values.
	 */
	static class Item
	{
		/** A supported annotation element. */
		final TypeElement annotationElement;

		/** An annotated type element. */
		final TypeElement annotatedTypeElement;

		/**
		 * A map that associates an annotation's elements with their
		 * corresponding values, either declared or default.
		 */
		final Map<String, String> elements;

		/**
		 * Constructs a new {@code Item} object.
		 *
		 * @param annotationElement a supported annotation element
		 * @param annotatedTypeElement an annotated type element
		 * @param elements a map that associates an annotation's
		 *	elements with their corresponding values, either
		 *	declared or default
		 */
		Item(TypeElement annotationElement,
					TypeElement annotatedTypeElement,
					Map<String, String> elements)
		{
			this.annotationElement = Objects.requireNonNull(
				annotationElement, "annotationElement");
			this.annotatedTypeElement = Objects.requireNonNull(
				annotatedTypeElement, "annotatedTypeElement");
			this.elements = Objects.requireNonNull(elements,
								"elements");
		}

		@Override
		public String toString()
		{
			return new StringBuilder(512)
				.append("(")
				.append(annotationElement)
				.append(" : ")
				.append(annotatedTypeElement)
				.append(" : ")
				.append(elements)
				.append(")")
				.toString();
		}
	}

	/**
	 * This class serves for aggregation of supported annotation processor
	 * options.
	 */
	static class ProcessorOption
	{
		/** The name of the ascriptive option. */
		static final String OPTION_ASCRIPTIVE =
					"org.zzzyxwvut.loggable.ascriptive";

		/** The name of the build-time option. */
		static final String OPTION_BUILD_TIME =
					"org.zzzyxwvut.loggable.build.time";

		/** The name of the concurrent option. */
		static final String OPTION_CONCURRENT =
					"org.zzzyxwvut.loggable.concurrent";

		/** The name of the verbose option. */
		static final String OPTION_VERBOSE =
					"org.zzzyxwvut.loggable.verbose";

		/** An option for generating attributes to aid testing. */
		final boolean ascriptive;

		/**
		 * An option for running concurrently parts of processing and
		 * generating.
		 */
		final boolean concurrent;

		/** An option for reporting processing diagnostics. */
		final boolean verbose;

		/**
		 * An option for including build time-stamps for generated
		 * sources.
		 *
		 * @see javax.annotation.processing.Generated#date
		 */
		final String buildTime;

		/**
		 * Constructs a new {@code ProcessorOption} object.
		 *
		 * @param options a map of passed supported processor options
		 */
		ProcessorOption(Map<String, String> options)
		{
			Objects.requireNonNull(options, "options");
			ascriptive = Boolean.parseBoolean(
						Objects.requireNonNullElse(
				options.get(OPTION_ASCRIPTIVE), "false"));
			concurrent = Boolean.parseBoolean(
						Objects.requireNonNullElse(
				options.get(OPTION_CONCURRENT), "false"));
			verbose = Boolean.parseBoolean(
						Objects.requireNonNullElse(
				options.get(OPTION_VERBOSE), "false"));
			buildTime = options.get(OPTION_BUILD_TIME);
		}
	}

	/** This class represents a processing context. */
	static class Context
	{
		/** An annotation processor. */
		final Processor processor;

		/** Passed supported processor options. */
		final ProcessorOption options;

		/** A processing environment. */
		final ProcessingEnvironment processingEnv;

		/**
		 * Constructs a new {@code Context} object.
		 *
		 * @param processor an annotation processor
		 * @param options passed supported processor options
		 * @param processingEnv a processing environment
		 */
		Context(Processor processor, ProcessorOption options,
					ProcessingEnvironment processingEnv)
		{
			this.processor = Objects.requireNonNull(processor,
								"processor");
			this.options = Objects.requireNonNull(options,
								"options");
			this.processingEnv = Objects.requireNonNull(
					processingEnv, "processingEnv");
		}
	}
}
