/**
 * Provides annotations that extend Java platform's core logging facilities.
 * <p>
 * A sample of a child-module {@code pom.xml}:
 *
 * <pre> {@code <project ...>
 *     <modelVersion>4.0.0</modelVersion>
 *     <artifactId>...</artifactId>
 *     <parent>
 *         <groupId>...</groupId>
 *         <artifactId>...</artifactId>
 *         <version>...</version>
 *     </parent>
 *     <properties>
 *         <!-- ISO 8601 -->
 *         <maven.build.timestamp.format>
 *             yyyy-MM-dd'T'HH:mm:ss.SSSXX
 *         </maven.build.timestamp.format>
 *         <julics.version>1.0-SNAPSHOT</julics.version>
 *     </properties>
 *     <dependencies>
 *         <dependency>
 *             <groupId>org.zzzyxwvut</groupId>
 *             <artifactId>julics-core</artifactId>
 *             <version>}<code>${julics.version}</code>{@code </version>
 *         </dependency>
 *         <dependency>
 *             <groupId>org.zzzyxwvut</groupId>
 *             <artifactId>julics-naming</artifactId>
 *             <version>}<code>${julics.version}</code>{@code </version>
 *         </dependency>
 *         <dependency>
 *             <groupId>org.zzzyxwvut</groupId>
 *             <artifactId>julics-annotations</artifactId>
 *             <version>}<code>${julics.version}</code>{@code </version>
 *             <optional>true</optional>
 *         </dependency>
 *     </dependencies>
 *     <build>
 *         <plugins>
 *             <plugin>
 *                 <groupId>org.apache.maven.plugins</groupId>
 *                 <artifactId>maven-dependency-plugin</artifactId>
 *                 <version>}<code>${maven-dependency-plugin.version}</code>{@code </version>
 *                 <executions>
 *                     <execution>
 *                         <goals>
 *                             <goal>properties</goal>
 *                         </goals>
 *                     </execution>
 *                 </executions>
 *             </plugin>
 *             <plugin>
 *                 <groupId>org.apache.maven.plugins</groupId>
 *                 <artifactId>maven-compiler-plugin</artifactId>
 *                 <configuration>
 *                     <compilerArgs combine.children="append">
 *                         <arg>-Xlint:all,-processing</arg>
 *                         <arg>-implicit:class</arg>
 *                         <arg>-XprintProcessorInfo</arg>
 *                         <arg>-XprintRounds</arg>
 *                         <arg>-Aorg.zzzyxwvut.loggable.build.time=}<code>${maven.build.timestamp}</code>{@code </arg>
 *                         <arg>-Aorg.zzzyxwvut.loggable.concurrent=true</arg>
 *                         <arg>-Aorg.zzzyxwvut.loggable.verbose=false</arg>
 *                         <arg>--processor-module-path</arg>
 *                         <arg>}<code>${org.zzzyxwvut:julics-annotations:jar}${path.separator}${org.zzzyxwvut:julics-naming:jar}${path.separator}${org.zzzyxwvut:julics-core:jar}</code>{@code </arg>
 *                     </compilerArgs>
 *                 </configuration>
 *             </plugin>
 *         </plugins>
 *     </build>
 * </project>}</pre><p>
 * The default value for boolean processor options, {@code concurrent} and
 * {@code verbose}, is {@code false}. The default value for {@code build.time}
 * is {@code null}.
 * <p>
 * In order to process annotations on the class path, dispense with
 * the {@code --processor-module-path} option (see {@code compilerArgs} above)
 * and its argument, along with the need of the {@code properties} goal of
 * {@code maven-dependency-plugin}, and add the following settings for
 * the {@code maven-compiler-plugin} configuration:
 * <pre> {@code <annotationProcessorPaths>
 *     <path>
 *         <groupId>org.zzzyxwvut</groupId>
 *         <artifactId>julics-annotations</artifactId>
 *         <version>}<code>${julics.version}</code>{@code </version>
 *     </path>
 * </annotationProcessorPaths>
 * <annotationProcessors>
 *     <annotationProcessor>
 *         org.zzzyxwvut.julics.processor.JulicsProcessor
 *     </annotationProcessor>
 * </annotationProcessors>}</pre><p>
 * A sample of a {@code module-info} declaration:
 * <pre><code>
 * module org.example.foo
 * {
 *     requires static org.zzzyxwvut.julics.annotations;
 *
 *     requires org.zzzyxwvut.julics.core;
 *     requires org.zzzyxwvut.julics.naming;
 * }</code></pre><p>
 * A sample of a class:
 * <pre><code>
 * package org.echoer;
 *
 * import java.util.Arrays;
 * import java.util.function.Consumer;
 * import java.util.function.Function;
 * import java.util.function.Supplier;
 * import java.util.stream.Collectors;
 *
 * import java.util.logging.Level;
 *
 * import org.zzzyxwvut.julics.annotation.Loggable;
 * import org.zzzyxwvut.julics.naming.LoggerRef;
 *
 * &#64;Loggable(retainsValues = false)
 * public class MainLauncher
 * {
 *	private static final Consumer&lt;Supplier&lt;String&gt;&gt; FINER =
 *							LoggerRef.logger()
 *		.apply(new LoggerRef(MainLauncher.class))
 *		.apply(Level.FINE);
 *
 *	public static void main(String[] args)
 *	{
 *		FINER.accept(Function.&lt;Supplier&lt;String&gt;&gt;identity()
 *			.&lt;String[]&gt;compose(args_ -&gt; () -&gt; Arrays.stream(args_)
 *				.collect(Collectors.joining(" ")))
 *			.apply(args));
 *	}
 * }</code></pre><p>
 * A sample of a {@code logging.properties}:
 * <pre><code>
 * org.echoer.MainLauncher.level=FINE
 * org.echoer.MainLauncher.handlers=java.util.logging.ConsoleHandler
 * org.echoer.MainLauncher.useParentHandlers=false
 *
 * java.util.logging.SimpleFormatter.format=%1$tT.%&lt;tN %4$s [%3$s] %5$s%n%6$s
 *
 * ## ConsoleHandler defaults to Level.INFO.
 * java.util.logging.ConsoleHandler.level=FINE
 * java.util.logging.ConsoleHandler.formatter=java.util.logging.SimpleFormatter
 * java.util.logging.ConsoleHandler.encoding=UTF-8
 * </code></pre>
 */
package org.zzzyxwvut.julics.annotation;
