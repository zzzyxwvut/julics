/**
 * Defines annotations that extend Java platform's core logging facilities.
 * <p>
 * This module needs to be only present at compile time (static). To maintain
 * their readability, its dependences are declared transitive either for
 * compilation of generated classes (cf. {@code @javax.annotation.processing.Generated})
 * or for testing on the class path (and thus preventing pollution of other
 * modules' declarations).
 *
 * @provides javax.annotation.processing.Processor
 */
@SuppressWarnings("module")
module org.zzzyxwvut.julics.annotations
{
	requires transitive java.compiler;
	requires transitive org.zzzyxwvut.julics.core;
	requires transitive org.zzzyxwvut.julics.naming;

	exports org.zzzyxwvut.julics.annotation;

	exports org.zzzyxwvut.julics.processor to
		org.zzzyxwvut.julics.annotations.tests;

	provides javax.annotation.processing.Processor with
		org.zzzyxwvut.julics.processor.JulicsProcessor;
}
