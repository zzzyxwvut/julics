package org.zzzyxwvut.julics.naming.internal;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

public class ClassNamerTests
{
	@SuppressWarnings("ThrowableResultIgnored")
	@Test
	public void testGiveWithEmptyName()
	{
		assertThrows(IllegalArgumentException.class, () ->
						ClassNamer.give(""));
	}

	@Test
	public void testGiveWithSingleComponentPackageOnly()
	{
		final String expected = "foo.GeneratedLoggingConfigurer";
		final String obtained = ClassNamer.give("foo");
		assertEquals(expected, obtained);
	}

	@Test
	public void testGiveWithThreeComponentPackageOnly()
	{
		final String expected = "foo.bar.baz.GeneratedLoggingConfigurer";
		final String obtained = ClassNamer.give("foo.bar.baz");
		assertEquals(expected, obtained);
	}

	@Test
	public void testGiveWithUnnamedPackageTopClass()
	{
		final String expected = "GeneratedFooLoggingConfigurer";
		final String obtained = ClassNamer.give("Foo");
		assertEquals(expected, obtained);
	}

	@Test
	public void testGiveWithPackageAndClass()
	{
		final String expected = "org.GeneratedFooLoggingConfigurer";
		final String obtained = ClassNamer.give("org.Foo");
		assertEquals(expected, obtained);
	}

	@Test
	public void testGiveWithUnnamedPackageThreeComponentNestedClass()
	{
		final String expected = "GeneratedFooBarBazLoggingConfigurer";
		final String obtained = ClassNamer.give("Foo.Bar.Baz");
		assertEquals(expected, obtained);
	}

	@Test
	public void testGiveWithUnnamedPackageLocalClass()
	{
		final String expected = "GeneratedLocal$1FooLoggingConfigurer";
		final String obtained = ClassNamer.give("Local$1Foo");
		assertEquals(expected, obtained);
	}

	@Test
	public void testGiveWithUnnamedPackageAnonymousClass()
	{
		final String expected = "GeneratedAnonymous$1LoggingConfigurer";
		final String obtained = ClassNamer.give("Anonymous$1");
		assertEquals(expected, obtained);
	}

	@Test
	public void testSimpleGivenNameWithPackageAndClass()
	{
		final String expected = "GeneratedFooLoggingConfigurer";
		final String obtained = ClassNamer.simpleGivenName(
					"org.GeneratedFooLoggingConfigurer");
		assertEquals(expected, obtained);
	}

	@Test
	public void testSimpleGivenNameWithUnnamedPackageTopClass()
	{
		final String expected = "GeneratedFooLoggingConfigurer";
		final String obtained = ClassNamer.simpleGivenName(
					"GeneratedFooLoggingConfigurer");
		assertEquals(expected, obtained);
	}
}
