/** Defines support for logger configuration naming and look-up. */
@SuppressWarnings("module")
module org.zzzyxwvut.julics.naming
{
	requires transitive java.logging;

	exports org.zzzyxwvut.julics.naming;

	exports org.zzzyxwvut.julics.naming.internal to
		org.zzzyxwvut.julics.annotations,
		org.zzzyxwvut.julics.annotations.tests;
}
