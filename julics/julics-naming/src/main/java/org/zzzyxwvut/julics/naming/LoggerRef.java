package org.zzzyxwvut.julics.naming;

import java.util.Objects;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.zzzyxwvut.julics.naming.internal.ClassNamer;

/**
 * Instances of this class force initialisation of generated logging configurer
 * classes and expose loggers of the classes whose declarations are annotated
 * with {@code org.zzzyxwvut.julics.annotation.Loggable}.
 */
public class LoggerRef
{
	private final String klassName;

	/**
	 * Constructs a new {@code LoggerRef} object and initialises
	 * a generated logging configurer class, if found.
	 *
	 * @param klass a class whose declaration is annotated with
	 *	{@code org.zzzyxwvut.julics.annotation.Loggable}
	 * @throws IllegalStateException if the {@code klass} is local or
	 *	anonymous, or a generated class is not found
	 */
	public LoggerRef(Class<?> klass)
	{
		Objects.requireNonNull(klass, "logger klass");

		if (klass.isLocalClass() || klass.isAnonymousClass())
			throw new IllegalStateException(
				"Local and anonymous classes are not supported");

		/*
		 * Observe the asymmetrical use of Class.getCanonicalName()
		 * and Class.getName() for logger name resolution.
		 *
		 * The latter method should be used to supply the argument for
		 * any Logger.getLogger(String) invocation so that logger names
		 * for nested-class loggers, if any, would not pollute
		 * hierarchical namespaces of dot-separated property keys.
		 *
		 * E.g. for an org.example.Foo.Bar.Baz logger define
		 *	org.example.Foo$Bar$Baz.level=INFO
		 *	org.example.Foo$Bar$Baz.useParentHandlers=false
		 *
		 * However, generated class naming depends on the canonical
		 * name of an annotated class representation obtained from
		 * java.compiler/javax.lang.model.element.TypeElement#getQualifiedName(),
		 * so the former method should be used to supply the argument
		 * for any ClassNamer.give(String) invocation. (Regardless of
		 * annotated class membership, a generated class is always
		 * a top level class.)
		 */
		klassName = klass.getName();
		String generatedKlassName = ClassNamer.give(
						klass.getCanonicalName());

		if (klass.isMemberClass()) {
			final String packageName = klass.getPackageName();
			final String klassTail = generatedKlassName.substring(
						packageName.length() + 1);

			/* Should name _giving_ opt for membership... */
			if (generatedKlassName.regionMatches(0, packageName,
						0, packageName.length())
					&& klassTail.contains(".")) {
				final String memberName = klassTail
					.replace('.', '$');
				generatedKlassName = new StringBuilder(1
						+ packageName.length()
						+ memberName.length())
					.append(packageName)
					.append(".")
					.append(memberName)
					.toString();
			}
		}

		try {	/* For a member class, '$' is expected. */
			Class.forName(generatedKlassName, true,
					klass.getModule().getClassLoader());
		} catch (final ClassNotFoundException e) {
			throw new IllegalStateException(e);
		}
	}

	/**
	 * Finds a logger.
	 *
	 * @return an optional with the found logger or an empty optional
	 */
	public Optional<Logger> getLogger()
	{
		return Optional.ofNullable(Logger.getLogger(klassName));
	}

	/**
	 * Returns a functional interface that takes a logger reference and
	 * returns a functional interface that takes a logger level and
	 * returns a functional interface that takes a throwable and
	 * returns a functional interface that takes a message and, as
	 * a side effect, logs the message with the associated throwable and
	 * does not return a value.
	 *
	 * @implSpec
	 * This implementation invokes {@link #getLogger()}.
	 *
	 * @return a curried function
	 * @see java.util.logging.Logger#log(Level, Throwable, Supplier)
	 */
	public static Function<LoggerRef,
				Function<Level,
				Function<Throwable,
				Consumer<Supplier<String>>>>> errorLogger()
	{
		return loggerRef -> level -> error -> message -> loggerRef
			.getLogger()
			.ifPresent(errorLogCaller()
					.apply(level)
					.apply(error)
					.apply(message));
	}

	private static Function<Level,
				Function<Throwable,
				Function<Supplier<String>,
				Consumer<Logger>>>> errorLogCaller()
	{
		return level -> error -> message -> logger ->
				logger.log(level, error, message);
	}

	/**
	 * Returns a functional interface that takes a logger reference and
	 * returns a functional interface that takes a logger level and
	 * returns a functional interface that takes a message and, as
	 * a side effect, logs the message and does not return a value.
	 *
	 * @implSpec
	 * This implementation invokes {@link #getLogger()}.
	 *
	 * @return a curried function
	 * @see java.util.logging.Logger#log(Level, Supplier)
	 */
	public static Function<LoggerRef,
				Function<Level,
				Consumer<Supplier<String>>>> logger()
	{
		return loggerRef -> level -> message -> loggerRef
			.getLogger()
			.ifPresent(logCaller()
					.apply(level)
					.apply(message));
	}

	private static Function<Level,
				Function<Supplier<String>,
				Consumer<Logger>>> logCaller()
	{
		return level -> message -> logger ->
				logger.log(level, message);
	}
}
