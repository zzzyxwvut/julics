package org.zzzyxwvut.julics.naming.internal;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/** This class exercises authority over name giving for class generation. */
public class ClassNamer
{
	private static final Pattern DOT = Pattern.compile("\\.");
	private static final String PREFIX = "Generated";
	private static final String SUFFIX = "LoggingConfigurer";

	private ClassNamer() { /* No instantiation. */ }

	/**
	 * Gives a new name derived from the passed class name.
	 * <p>
	 * (The "." separator is expected for nested types. Package components
	 * are assumed to begin with a lowercase letter, type names are assumed
	 * to begin with an uppercase letter.)
	 *
	 * @apiNote
	 * The necessity of taking the string representation of a class name,
	 * with a view to parsing it, originates from a character sequence
	 * representation of a type imposed by the implementations of
	 * javax.lang.model.element.QualifiedNameable. At run time, consider
	 * using Class#getCanonicalName() to supply an argument.
	 *
	 * @param className a fully-qualified canonical class name
	 * @return a new name derived from the passed class name
	 */
	public static String give(String className)
	{
		if (Objects.requireNonNull(className, "className").isBlank())
			throw new IllegalArgumentException("Blank class name");

		final Map<Boolean, List<String>> horns = Stream.of(
					Stream.of(PREFIX),
					DOT.splitAsStream(className)
						.filter(Predicate.not(
							String::isBlank)),
					Stream.of(SUFFIX))
			.flatMap(Function.identity())
			.collect(Collectors.partitioningBy(s ->
				Character.isUpperCase(s.codePointAt(0))));
		final List<String> packageParts = horns.get(false);
		final List<String> klasses = horns.get(true);
		return Stream.of(String.join(".", packageParts),
					String.join("", klasses))
			.collect(Collectors.joining((packageParts.isEmpty())
								? "" : "."));
	}

	/**
	 * Returns a simple (non-qualified) given class name.
	 * <p>
	 * (The "." separator is assumed for a nested type.)
	 *
	 * @param givenName a given class name
	 * @return a simple given class name
	 */
	public static String simpleGivenName(String givenName)
	{
		Objects.requireNonNull(givenName, "givenName");
		return givenName.substring(givenName.lastIndexOf('.') + 1);
	}
}
