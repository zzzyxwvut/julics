/** Defines test samples for annotation processing. */
module org.zzzyxwvut.julics.annotations.tests
{
	requires static org.zzzyxwvut.julics.annotations;

	requires org.zzzyxwvut.julics.core;
	requires org.zzzyxwvut.julics.naming;

	/* Make internal handlers accessible. */
	exports org.zzzyxwvut.julics.annotation.test to
		java.logging;

	/* Make encapsulated properties locatable. */
	opens org.zzzyxwvut.julics.annotation.test to
		org.zzzyxwvut.julics.core;
}
