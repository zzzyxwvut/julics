package org.zzzyxwvut.julics.annotation.test;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import java.util.logging.Logger;

import org.zzzyxwvut.julics.annotation.Loggable;
import org.zzzyxwvut.julics.core.LoggingConfiguration;
import org.zzzyxwvut.julics.processor.JulicsProcessor.JeNeSaisQuoi;

/** This class and its members are samples used for annotation processing. */
class Буквица
{
	private Буквица() { /* No instantiation. */ }

	@Nonce
	@JeNeSaisQuoi
	@Loggable(resource = "/private/а҆́зъ.properties")
	static class А҆́зъ { }

	@Nonce
	@JeNeSaisQuoi(comment = "any")
	@Loggable(retainsValues = false, resource = "/private/бꙋ́ки.properties")
	static class Бꙋ́ки { }

	@Nonce
	@JeNeSaisQuoi(none = false)
	@Loggable(configClass = NoOpConfiguration.class,
					resource = "/private/вѣ́ди.properties")
	static class Вѣ́ди { }

	@Nonce
	@JeNeSaisQuoi
	@Loggable(resource = "/private/глаго́ль.properties")
	static class Глаго́ль { }

	static class Добро̀
	{
		@Nonce
		@JeNeSaisQuoi(none = true)
		@Loggable(retainsValues = false,
				resource = "/private/добро̀-красная.properties")
		static class Красная { }
	}

	static class Є҆́сть
	{
		@Nonce
		@JeNeSaisQuoi(none = true)
		@Loggable(retainsValues = false,
				resource = "/private/є҆́сть-красная.properties")
		static class Красная { }
	}

	static final class NoOpConfiguration extends LoggingConfiguration
	{
		NoOpConfiguration() { }

		@Override
		protected void onRead(Logger logger) { /* No-op. */ }
	}

	@Nonce
	@JeNeSaisQuoi
	@Loggable(resource = "/private/support.properties")
	enum Support { NONCE }

	@Nonce
	@JeNeSaisQuoi
	@Loggable
	interface Unavailable { }

	@Nonce
	@JeNeSaisQuoi
	@Loggable
	@interface Unprovidable { }

	@Target(ElementType.TYPE)
	@Retention(RetentionPolicy.RUNTIME)
	@interface Nonce { }
}
