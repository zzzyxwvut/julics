package org.zzzyxwvut.julics.annotation.test;

import java.util.logging.ConsoleHandler;

/** A {@code ConsoleHandler} alias. */
public class StandardErrorConsoleHandler extends ConsoleHandler
{
	/** Constructs a new {@code StandardErrorConsoleHandler} object. */
	public StandardErrorConsoleHandler() { super(); }
}
