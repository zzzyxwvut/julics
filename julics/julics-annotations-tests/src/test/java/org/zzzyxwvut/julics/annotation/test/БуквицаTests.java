package org.zzzyxwvut.julics.annotation.test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertIterableEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assumptions.assumeTrue;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UncheckedIOException;
import java.lang.invoke.MethodHandles;
import java.util.AbstractMap.SimpleImmutableEntry;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import java.util.logging.Level;
import java.util.logging.LogManager;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.function.Executable;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import org.zzzyxwvut.julics.annotation.Loggable;
import org.zzzyxwvut.julics.core.LoggingConfiguration;
import org.zzzyxwvut.julics.naming.LoggerRef;
import org.zzzyxwvut.julics.naming.internal.ClassNamer;

@Loggable
public class БуквицаTests
{
	private static final LogManager MANAGER = LogManager.getLogManager();
	private static final Consumer<Supplier<String>> INFOER =
							LoggerRef.logger()
		.apply(new LoggerRef(БуквицаTests.class))
		.apply(Level.INFO);

	private static String classPathSource(String prefixPath,
					Class<?> klass, String fileSuffix)
	{
		/*
		 * See the "generatedSourcesDirectory" value of
		 * the maven-compiler-plugin for the prefixPath.
		 */
		return new StringBuilder(128)
			.append(prefixPath)
			.append(ClassNamer.give(klass.getCanonicalName())
							.replace(".", "/"))
			.append(fileSuffix)
			.toString();
	}

	private static byte[] readUpTo1024(String sourceName, Class<?> klass)
	{
		try (InputStream is = klass.getResourceAsStream(sourceName)) {
			if (is == null)
				throw new IllegalArgumentException(
					String.format("Unavailable resource: '%s'",
							sourceName));

			try (BufferedInputStream bis =
					new BufferedInputStream(is, 1024)) {
				final byte[] out = new byte[bis.available()];
				bis.read(out);
				return out;
			}
		} catch (final IOException e) {
			throw new UncheckedIOException(e);
		}
	}

	private static boolean isR(Class<?> target) throws Throwable
	{
		final Class<?> klass = Class.forName(ClassNamer.give(
						target.getCanonicalName()),
				false, target.getModule().getClassLoader());
		return (boolean) MethodHandles
			.lookup()
			.findStaticGetter(klass, "R", boolean.class)
			.invokeExact();
	}

	private static Function<String,
				Function<String, Map.Entry<String, String>>>
								proprietor()
	{
		return klassName -> propertyName -> {
			final String name = String.format(propertyName,
								klassName);
			return new SimpleImmutableEntry<>(name,
				Objects.requireNonNullElse(
					MANAGER.getProperty(name), ""));
		};
	}

	private static Map<String, String> properties(String klassName)
	{
		return Stream.of("%s.level", "%s.handlers",
				"%s.handlers.ensureCloseOnReset",
				"%s.useParentHandlers")
			.map(proprietor()
				.apply(klassName))
			.collect(Collectors.collectingAndThen(Collectors.toMap(
					Map.Entry::getKey, Map.Entry::getValue,
					(oldValue, newValue) -> newValue,
							LinkedHashMap::new),
				Collections::unmodifiableMap));
	}

	@AfterAll
	public static void tearDownAll()
	{
		INFOER.accept(() -> LoggingConfiguration
				.getRegisteredLoggerNames()
			.stream()
			.sorted()
			.collect(Collectors.toList()).toString());
		INFOER.accept(() -> MANAGER.getProperty(
					"org.zzzyxwvut.julics.nonce"));
	}

	@ParameterizedTest
	@ValueSource(classes = {
		Буквица.Support.class,
		Буквица.А҆́зъ.class, Буквица.Бꙋ́ки.class,
		Буквица.Вѣ́ди.class, Буквица.Глаго́ль.class,
		Буквица.Добро̀.Красная.class, Буквица.Є҆́сть.Красная.class
	})
	public void testPropertiesOfEachLoggableClass(Class<?> klass)
							throws Throwable
	{
		/*
		 * This test is arranged as follows:
		 * (*) for RejectingPolicy classes, two complete sets of
		 *	properties (before, after) are defined with
		 *	non-matching paired values;
		 * (*) for AcceptingPolicy classes, the after complete set
		 *	is defined, whereas the before set is dynamically
		 *	assembled with the undefined properties of interest
		 *	made resolved with empty string values;
		 */
		final String klassName = klass.getName();
		final Map<String, String> before = properties(klassName);
		final boolean rejectable = isR(klass);
		final LoggerRef ref = new LoggerRef(klass);
		final Map<String, String> after = (rejectable)
			? properties(klassName)
			: properties(klassName)
				.entrySet()
				.stream()
				.collect(Collectors.collectingAndThen(
					Collectors.toMap(Map.Entry::getKey,
						entry -> (entry.getValue()
								.isEmpty())
							? "<FIXME: unexpected>"
							: "",
						(oldValue, newValue) -> newValue,
						LinkedHashMap::new),
					Collections::unmodifiableMap));
		assertIterableEquals(before.entrySet(), after.entrySet());
		assumeTrue(ref.getLogger().isPresent());
	}

	@ParameterizedTest
	@ValueSource(classes = {
		Буквица.Support.class,
		Буквица.А҆́зъ.class, Буквица.Бꙋ́ки.class,
		Буквица.Вѣ́ди.class, Буквица.Глаго́ль.class,
		Буквица.Добро̀.Красная.class, Буквица.Є҆́сть.Красная.class
	})
	public void testBytesOfEachLoggableClass(Class<?> klass)
	{
		final String expectedName = classPathSource(
					"/samples/", klass, ".java.sample");
		final String obtainedName = classPathSource(
					"/generated-sources/", klass, ".java");
		final byte[] expectedSrc = readUpTo1024(expectedName, klass);
		final byte[] obtainedSrc = readUpTo1024(obtainedName, klass);
		assertArrayEquals(expectedSrc, obtainedSrc);
	}

	private static Function<Class<?>, Executable> klasser()
	{
		return klass -> () -> Class.forName(ClassNamer.give(
						klass.getCanonicalName()),
				true, klass.getModule().getClassLoader());
	}

	@SuppressWarnings("ThrowableResultIgnored")
	@ParameterizedTest
	@ValueSource(classes = {
		Буквица.Unavailable.class, Буквица.Unprovidable.class
	})
	public void testUnsupportedTypeTarget(Class<?> klass)
	{
		assertThrows(ClassNotFoundException.class,
						klasser().apply(klass));
	}
}
