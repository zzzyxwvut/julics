A convenient means to use Java platform's core logging facilities.

In particular, the `julics-annotations` module offers automatic updating
of logging configuration, read from a resource file, via annotation.
See [examples](https://bitbucket.org/zzzyxwvut/julics/src/master/julics/julics-annotations/src/main/java/org/zzzyxwvut/julics/annotation/package-info.java "examples") of defined components.

This project depends on `antics-maven-plugin` available from
[here](https://bitbucket.org/zzzyxwvut/antics-maven-plugin.git "here").
Before building this project, make a local installation of the plugin, i.e.  
`cd repos/`  
`git clone https://bitbucket.org/zzzyxwvut/antics-maven-plugin.git`  
`cd antics-maven-plugin/antics-maven-plugin/`  
`mvn install`
